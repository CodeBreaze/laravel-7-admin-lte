<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'login','uses'=>'LoginController@login']);
Route::get('dashboard', ['as'=>'dashboard','uses'=>'DashboardController@dashboard']);
Route::group(['prefix'=>'users','as'=>'users.'],function(){
    Route::get('/', ['as'=>'list','uses'=>'UserController@list']);
    Route::get('new', ['as'=>'new','uses'=>'UserController@new']);
});

