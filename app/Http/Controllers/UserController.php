<?php

namespace App\Http\Controllers;

class UserController
{
    public function list(){
        return view('users.list');
    }

    public function new(){
        return view('users.new');
    }
}
